package com.quakbo.hw3_1;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.MorphiaIterator;
import org.mongodb.morphia.query.Query;

/**
 * Write a program in the language of your choice that will remove the lowest homework score for each student. Since
 * there is a single document for each student containing an array of scores, you will need to update the scores array
 * and remove the homework.
 * <p>
 * Remember, just remove a homework score. Don't remove a quiz or an exam!
 * <p>
 * Hint/spoiler: With the new schema, this problem is a lot harder and that is sort of the point. One way is to find the
 * lowest homework in code and then update the scores array with the low homework pruned.
 * <p>
 * {
 *   "_id" : 0,
 *   "name" : "aimee Zank",
 *   "scores" : [
 *     {
 *       "type" : "exam",
 *       "score" : 1.463179736705023
 *     },
 *     {
 *       "type" : "quiz",
 *       "score" : 11.78273309957772
 *     },
 *     {
 *       "type" : "homework",
 *       "score" : 6.676176060654615
 *     },
 *     {
 *       "type" : "homework",
 *       "score" : 35.8740349954354
 *     }
 *   ]
 * }
 */
public class Homework_3_1
{
	public static void main(String[] args)
	{
		final Morphia morphia = new Morphia();
		morphia.mapPackage("com.quakbo.hw3_1");

		MongoClient mongoClient = new MongoClient();
		final Datastore ds = morphia.createDatastore(mongoClient, "school");
		ds.ensureIndexes();

		Query<Student> query = ds.createQuery(Student.class);
		Iterable<Student> fetch = query.fetch();

		try
		{
			for (Student student : fetch)
			{
				int lowestHomeworkIndex = -1;
				double lowestHomeworkScore = Double.MAX_VALUE;
				for (int i = 0; i < student.getScores().size(); i++)
				{
					Score score = student.getScores().get(i);

					if ("homework".equals(score.getType()))
					{
						if (lowestHomeworkIndex == -1 || score.getScore() < lowestHomeworkScore)
						{
							lowestHomeworkIndex = i;
							lowestHomeworkScore = score.getScore();
						}
					}
				}

				if (lowestHomeworkIndex > -1)
				{
					student.getScores().remove(lowestHomeworkIndex);
					ds.save(student);
				}
			}
		}
		finally
		{
			((MorphiaIterator) fetch).close();
			mongoClient.close();
		}
	}
}

