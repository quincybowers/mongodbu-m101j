package com.quakbo.hw3_1;

import java.util.ArrayList;
import java.util.List;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;

@Entity(value = "students", noClassnameStored = true)
@Indexes({
		@Index(value = "id", name = "byId"),
		@Index(value = "name", name = "byName")
})
public class Student
{
//{
//	"_id":137,
//	"name":"Tamika Schildgen",
//	"scores":[
//		{
//			"type":"exam",
//			"score":4.433956226109692
//		},
//		{
//			"type":"quiz",
//			"score":65.50313785402548
//		},
//		{
//			"type":"homework",
//			"score":89.5950384993947
//		},
//		{
//			"type":"homework",
//			"score":54.75994689226145
//		}
//	]
//}

	@Id
	public int id;
	public String name;
	@Embedded
	public ArrayList<Score> scores;

	public Student()
	{
	}

	public Student(int id)
	{
		this.id = id;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<Score> getScores()
	{
		return scores;
	}

	public void setScores(ArrayList<Score> scores)
	{
		this.scores = scores;
	}

	@Override
	public String toString()
	{
		return "Student{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", scores=" + scores +
				'}';
	}
}
