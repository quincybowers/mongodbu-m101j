package main;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Projections.*;

/**
 *  Write a program in the language of your choice that will remove the grade of type "homework" with the lowest score
 *  for each student from the dataset that you imported in the previous homework. Since each document is one grade, it
 *  should remove one document per student.
 *
 *  Hint/spoiler: If you select homework grade-documents, sort by student and then by score, you can iterate through and
 *  find the lowest score for each student by noticing a change in student id. As you notice that change of student_id
 *  remove the document.
 */
public class Homework2_3 {
    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient();
        MongoDatabase mongoDatabase = mongoClient.getDatabase("students");
        MongoCollection<Document> collection = mongoDatabase.getCollection("grades");

        Bson whereTypeIsHomework = Filters.eq("type", "homework");
        Bson selectStudentScore = fields(include("student_id", "score"));
        Bson byStudentThenScore = new Document("student_id", 1).append("score", 1);
        List<Document> results = collection.find(whereTypeIsHomework)
                .projection(selectStudentScore)
                .sort(byStudentThenScore)
                .into(new ArrayList<>());

        List<ObjectId> toRemove = Lists.newArrayList();
        int id = -1;
        for (Document doc : results) {
            int currId = doc.getInteger("student_id");
            if (currId > id) {
                id = currId;
                toRemove.add(doc.getObjectId("_id"));
            }
        }

        System.out.println(collection.deleteMany(Filters.in("_id", toRemove)).getDeletedCount());
        System.out.println(collection.count());
    }
}

